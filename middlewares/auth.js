const jwt = require('jsonwebtoken');
const errorMessages = require('../helpers/errorMessages');
const { errorResponse } = require('../helpers/response');

// eslint-disable-next-line consistent-return
const authenticateToken = (req, res, next) => {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(' ')[1];
  if (!token) {
    return res.json(errorResponse(errorMessages.UNAUTHORIZED, 'UNAUTHORIZED'));
  }
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      return res.json(errorResponse(errorMessages.BAD_ACCESS_TOKEN, 'BAD_ACCESS_TOKEN'));
    }
    req.user = user;
    return next();
  });
};

module.exports = authenticateToken;
