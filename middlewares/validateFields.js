const errorMessages = require('../helpers/errorMessages');
const { errorResponse } = require('../helpers/response');
const { parsePhone } = require('../helpers/utils');

const validateFields = (fieldsList = []) => (req, res, next) => {
  const fields = {
    ...(req.body || {}),
  };

  // eslint-disable-next-line no-restricted-syntax
  for (const key of fieldsList) {
    const value = fields[key];

    switch (key) {
      case 'PHONE':
        if (!/^\d{10}$/i.test(parsePhone(value))) {
          return res.json(errorResponse(errorMessages.BAD_PHONE_FORMAT, 'BAD_PHONE_FORMAT'));
        }
        break;
      case 'PASSWORD':
        if (!/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(value)) {
          return res.json(errorResponse(errorMessages.BAD_PASSWORD_FORMAT, 'BAD_PASSWORD_FORMAT'));
        }
        break;
      case 'NAME':
        if (!/^([а-яА-ЯЁё]+([. \-`']*|[. \-`']?([ivxlcIVXLC]?[.\-`']?))*?)?([ \-`']+[а-яА-ЯЁё]+[ \-`'.]*([ \-`'][ivxlcIVXLC]+)*)*$/i.test(value)) {
          return res.json(errorResponse(errorMessages.BAD_NAME_FORMAT, 'BAD_NAME_FORMAT'));
        }
        break;
      case 'EMAIL':
        // eslint-disable-next-line
        if (!/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(value)) {
          return res.json(errorResponse(errorMessages.BAD_EMAIL_FORMAT, 'BAD_EMAIL_FORMAT'));
        }
        break;
      default:
        break;
    }
  }

  return next();
};

module.exports = validateFields;
