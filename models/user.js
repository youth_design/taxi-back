const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  PHONE: { type: String, unique: true, required: true },
  PASSWORD: { type: String, required: true },
  NAME: { type: String, required: true },
  EMAIL: { type: String },
  createdOn: { type: Date, default: Date.now },
  updatedOn: { type: Date, default: Date.now },
});

const User = mongoose.model('User', userSchema);

module.exports = User;
