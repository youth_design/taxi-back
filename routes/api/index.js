const express = require('express');

const { successResponse } = require('../../helpers/response');

const { loginHandler, tokenHandler } = require('../../handlers/loginHandler');
const registerHandler = require('../../handlers/registerHandler');
const refsHandler = require('../../handlers/refsHandler');

const authenticateToken = require('../../middlewares/auth');
const validateFields = require('../../middlewares/validateFields');

const router = express.Router();

const posts = [
  {
    username: 'User1',
    title: 'User1',
  },
  {
    username: 'User1',
    title: 'User12',
  },
  {
    username: 'User2',
    title: 'User2',
  },
];

router.post('/login', validateFields([
  'PHONE',
]), loginHandler);
router.post('/token', tokenHandler);
router.get('/refs', authenticateToken, refsHandler);

router.post('/register', validateFields([
  'PASSWORD',
  'PHONE',
  'NAME',
  'EMAIL',
]), registerHandler);
router.get('/posts', authenticateToken, (req, res) => {
  res.json(successResponse({ posts: posts.filter((post) => post.username === req.user.name) }));
});

module.exports = router;
