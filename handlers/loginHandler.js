const jwt = require('jsonwebtoken');
const { errorResponse, successResponse } = require('../helpers/response');
const { comparePasswordWithHash } = require('../helpers/passwords');
const User = require('../models/user');
const errorMessages = require('../helpers/errorMessages');
const { parsePhone } = require('../helpers/utils');

const refreshTokens = [];

const generateAccessToken = (user) => jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '15m' });

const loginHandler = async (req, res) => {
  const { PHONE, PASSWORD } = req.body;

  const parsedPhone = parsePhone(PHONE);

  const userFromDb = await User.findOne({ PHONE: parsedPhone });

  if (
    !userFromDb
    || !(await comparePasswordWithHash(PASSWORD, userFromDb.PASSWORD))
  ) {
    return res.json(errorResponse(errorMessages.BAD_CREDENTIALS, 'BAD_CREDENTIALS'));
  }

  const user = {
    PHONE: parsedPhone,
    NAME: userFromDb.NAME,
  };

  const accessToken = generateAccessToken(user);
  const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET);
  refreshTokens.push(refreshToken);
  return res.json(successResponse({ accessToken, refreshToken }));
};

// eslint-disable-next-line consistent-return
const tokenHandler = (req, res) => {
  const refreshToken = req.body.token;
  if (!refreshToken) {
    return res.json(errorResponse(errorMessages.BAD_ACCESS_TOKEN, 'BAD_ACCESS_TOKEN'));
  }
  if (!refreshTokens.includes(refreshToken)) {
    return res.json(errorResponse(errorMessages.BAD_ACCESS_TOKEN, 'BAD_ACCESS_TOKEN'));
  }
  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
    if (err) {
      return res.json(errorResponse(errorMessages.UNAUTHORIZED, 'UNAUTHORIZED'));
    }
    const accessToken = generateAccessToken({ PHONE: user.PHONE, NAME: user.NAME });
    return res.json(successResponse({ accessToken }));
  });
};

module.exports = {
  loginHandler,
  tokenHandler,
};
