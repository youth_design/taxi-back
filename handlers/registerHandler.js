const { successResponse } = require('../helpers/response');
const User = require('../models/user');
const { hashPassword } = require('../helpers/passwords');
const Logger = require('../helpers/logger');
const { errorResponse } = require('../helpers/response');
const errorMessages = require('../helpers/errorMessages');
const { parsePhone } = require('../helpers/utils');

const registerHandler = async (req, res) => {
  const {
    PHONE, PASSWORD, NAME, EMAIL,
  } = req.body;
  const user = new User({
    PHONE: parsePhone(PHONE),
    PASSWORD: await hashPassword(PASSWORD),
    NAME,
    EMAIL,
  });

  try {
    const newUser = await user.save();
    Logger.log(`registerHandler:::: New user created: ${newUser}`);
    return res.json(successResponse({ NAME: newUser.NAME, PHONE: newUser.PHONE }));
  } catch (err) {
    Logger.log(`registerHandler:::: ${err}`);
    if (err.name === 'MongoError' && err.code === 11000) {
      return res.json(errorResponse(errorMessages.DUPLICATE_PHONE, 'DUPLICATE_PHONE'));
    }
    return res.json(errorResponse(errorMessages.SERVICE_UNAVAILABLE, 'SERVICE_UNAVAILABLE'));
  }
};

module.exports = registerHandler;
