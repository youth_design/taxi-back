const { successResponse } = require('../helpers/response');

const refs = {};
Object.entries(process.env)
  .filter(([key]) => /^REACT_/.test(key))
  .forEach(([key, value]) => {
    refs[key] = value;
  });

const refsHandler = (req, res) => res.json(successResponse(refs));

module.exports = refsHandler;
