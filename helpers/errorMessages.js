const errorMessages = {
  BAD_CREDENTIALS: 'Неверные данные для входа',
  BAD_PHONE_FORMAT: 'Неверный формат телефона',
  BAD_PASSWORD_FORMAT: 'Пароль должен состоять минимум из 8 символов, а также содержать 1 цифру и 1 букву',
  BAD_ACCESS_TOKEN: 'Неверный токен авторизации',
  BAD_REFRESH_TOKEN: 'Неверный токен авторизации',
  UNAUTHORIZED: 'Данная функция доступна только авторизированным пользователям',
  SERVICE_UNAVAILABLE: 'Сервис недоступен',
  BAD_NAME_FORMAT: 'Неверный формат имени',
  BAD_EMAIL_FORMAT: 'Неверный формат электронной почты',
  DUPLICATE_PHONE: 'Пользователь с таким телефоном уже существует',
};

module.exports = errorMessages;
