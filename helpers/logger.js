const chalk = require('chalk');

const getDateString = () => {
  const date = new Date();
  return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}:${date.getMilliseconds()}`;
};

const log = (message) => {
  if (process.env.COLORIZED_OUTPUT === '1') {
    console.log(`[${chalk.green(getDateString())}]: ${message}`);
  } else {
    console.log(`[${getDateString()}]: ${message}`);
  }
};

module.exports = {
  log,
};
