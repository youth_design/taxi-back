/**
 *
 * @param {string} phone
 */
const parsePhone = (phone) => {
  const parsedPhone = phone.replace(/^(\+38|38)/i, '');
  return parsedPhone.replace(/-\(\)\s/gi, '');
};

/**
 *
 * @param {string} phone
 */
const normalizePhone = (phone) => {
  if (phone.length !== 10) {
    throw new Error('Bad phone format');
  }
  return `+38${phone.slice(0, 1)} (${phone.slice(1, 3)}) ${phone.slice(3, 5)} ${phone.slice(5, 7)} ${phone.slice(7, 10)}`;
};

module.exports = {
  parsePhone,
  normalizePhone,
};
