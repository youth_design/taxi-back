module.exports = {
  errorResponse: (message, code) => JSON.stringify({
    error: {
      code: code || 500,
      message,
    },
  }, null, 4),
  successResponse: (data) => JSON.stringify({
    result: data,
  }, null, 4),
};
