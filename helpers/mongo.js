const mongoose = require('mongoose');
const Logger = require('./logger');

let db;

const getDB = async () => {
  if (db) {
    return db;
  }
  try {
    db = await mongoose.connect('mongodb://localhost/taxi', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
    Logger.log('Successfully connected to MongoDB');
    return db;
  } catch (err) {
    Logger.log('MongoDB connection error');
    Logger.log(err);
    return process.exit(1);
  }
};

module.exports = getDB;
