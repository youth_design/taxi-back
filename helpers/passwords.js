const bcrypt = require('bcrypt');

/**
 *
 * @param {string} password
 * @returns {Promise<string>}
 */
const hashPassword = async (password) => {
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(password, salt);
  return hash;
};

/**
 *
 * @param {string} password
 * @param {string} hash
 * @returns {Promise<boolean>}
 */
const comparePasswordWithHash = async (password, hash) => {
  const isSame = await bcrypt.compare(password, hash);
  return isSame;
};

module.exports = {
  hashPassword,
  comparePasswordWithHash,
};
